from migrate.versioning import api
from app import db, create_app
import os.path

app = create_app()

with app.app_context():
        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........
        db.create_all()

if not os.path.exists(app.config["SQLALCHEMY_MIGRATE_REPO"]):
    api.create(app.config["SQLALCHEMY_MIGRATE_REPO"], 'database repository')
    api.version_control(app.config["SQLALCHEMY_DATABASE_URI"], app.config["SQLALCHEMY_MIGRATE_REPO"])
else:
    api.version_control(app.config["SQLALCHEMY_DATABASE_URI"], app.config["SQLALCHEMY_MIGRATE_REPO"], api.version(app.config["SQLALCHEMY_MIGRATE_REPO"]))