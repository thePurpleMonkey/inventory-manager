from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, ValidationError
from wtforms.validators import Required, Email, Length, Regexp, EqualTo
from ..models import User

class LoginForm(Form):
	email = StringField("Email", validators=[Required(), Length(1, 64), Email()])
	password = PasswordField("Password", validators=[Required()])
	remember_me = BooleanField("Keep me logged in")
	submit = SubmitField("Log in")
	
class RegistrationForm(Form):
	email = StringField("Email", validators=[Required(), Length(1, 64), Email()])
	nickname = StringField("Nickname", validators=[Length(1, 64), Regexp("^[A-Za-z][A-Za-z0-9_.]*$", 0, "Nicknames may only contain letters, numbers, dots, or underscores")])
	password = PasswordField("Password", validators=[Required(), EqualTo("confirm", "Passwords must match")])
	confirm = PasswordField("Confirm password", validators=[Required()])
	submit = SubmitField("Register")
	
	def validate_email(self, field):
		if User.query.filter_by(email=field.data).first():
			raise ValidationError("Email already registered.")