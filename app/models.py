from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin
from . import login_manager

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class ProductOrder(db.Model):
	__tablename__ = "productorders"
	__table_args__ = (db.PrimaryKeyConstraint("order_id", "product_id"),)
	order_id   = db.Column(db.Integer, db.ForeignKey("orders.id"))
	product_id = db.Column(db.Integer, db.ForeignKey("products.id"))
	quantity   = db.Column(db.Integer)
	product    = db.relationship("Product", back_populates="orders")
	order      = db.relationship("Order", back_populates="products")

class Product(db.Model):
	__tablename__ = "products"
	id       = db.Column(db.Integer, primary_key=True)
	name     = db.Column(db.String(64), unique=True)
	quantity = db.Column(db.Integer, nullable=False, default=0)
	min      = db.Column(db.Integer, nullable=True)
	max      = db.Column(db.Integer, nullable=True)
	note     = db.Column(db.UnicodeText, nullable=True)
	owner_id = db.Column(db.Integer, db.ForeignKey("users.id"))
	orders   = db.relationship("ProductOrder", back_populates="product", cascade="save-update, merge, delete, delete-orphan")
	
	def __repr__(self):
		return "<Product '{}'>".format(self.name)

class Order(db.Model):
	__tablename__ = "orders"
	id       = db.Column(db.Integer, primary_key=True)
	date     = db.Column(db.DateTime, nullable=False)
	owner_id = db.Column(db.Integer, db.ForeignKey("users.id"))
	products = db.relationship("ProductOrder", back_populates="order", cascade="save-update, merge, delete, delete-orphan")
	
	def __repr__(self):
		return "<Order '{}'>".format(self.date)
		
class User(UserMixin, db.Model):
	__tablename__ = "users"
	id            = db.Column(db.Integer, primary_key=True)
	nickname      = db.Column(db.String(64), nullable=True)
	email         = db.Column(db.String(64), unique=True, index=True)
	password_hash = db.Column(db.String(128))
	orders        = db.relationship("Order", backref="owner")
	products      = db.relationship("Product", backref="owner")
	
	@property
	def password(self):
		raise AttributeError("password is not a readable attribute")
		
	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)
		
	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)
	
	def __repr__(self):
		return "<User '{}'>".format(self.email.split('@')[0])