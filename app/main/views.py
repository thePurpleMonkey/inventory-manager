from flask import render_template, redirect, url_for, flash, request, abort
from flask.ext.login import login_required, current_user
from flask.ext.mail import Message
from dateutil.parser import parse as date_parse

import json

from . import main
from .. import db, mail
from ..models import Product, Order, ProductOrder
from .forms import NewProductForm, ContactForm

@main.route("/")
def home():
	return render_template("index.html")
	
@main.route("/inventory")
@login_required
def inventory():
	products = Product.query.filter_by(owner=current_user).order_by(Product.name).all()
	orders = Order.query.filter_by(owner=current_user).order_by(Order.date).all()
	new_product_form = NewProductForm()

	return render_template("inventory.html", products=products, new_product_form=new_product_form, orders=orders, Order=Order)

@main.route("/js/inventory.js")
@login_required
def inventory_js():
	return render_template("inventory.js")
	
@main.route("/product/add", methods=["POST"])
@login_required
def add_product():
	form = NewProductForm()
	
	if form.validate_on_submit():
		name = form.name.data
		quantity = form.quantity.data
		min = form.min.data
		max = form.max.data
		
		p = Product(name=name, quantity=quantity, min=min, max=max, owner=current_user)
		db.session.add(p)
		db.session.commit()
		
		flash("Successfully added '{}' to product list.".format(name), "success")
		
	else:
		for fieldName, errorMessages in form.errors.iteritems():
			for err in errorMessages:
				# do something with your errorMessages for fieldName
				flash("Error on field '{}': {}.".format(fieldName, err), "danger")
	
	return redirect(url_for(".inventory"))
	
@main.route("/product/remove", methods=["POST"])
@login_required
def remove_product():
	p = Product.query.get(request.form["id"])
	if p is None:
		abort(404)
		
	db.session.delete(p)
	db.session.commit()
	
	flash("Successfully deleted {}".format(p.name), "success")
		
	return redirect(url_for(".inventory"))
	
@main.route("/product/details")
@login_required
def product_details():
	p = Product.query.get(request.args["id"])
	if p is None:
		abort(404)
	
	return render_template("details.html", product=p)
	
@main.route("/edit")
@login_required
def edit_product_page():
	p = Product.query.get(request.args["id"])
	if p is None:
		abort(404)
		
	return render_template("edit.html", product=p)
	
@main.route("/product/edit", methods=["POST"])
@login_required
def edit_product():
	p = Product.query.get(request.form["id"])
	if p is None:
		abort(404)
	
	try:
		name = request.form["name"]
		quantity = int(request.form["quantity"])
		min = int(request.form["min"]) if len(request.form["min"]) > 0 else None
		max = int(request.form["max"]) if len(request.form["max"]) > 0 else None
	except ValueError as e:
		raise
		flash("Could not read one or more submitted values. Make sure all input is correct and try again.", "danger")
		#flash(str(e), "danger")
		return redirect(url_for(".edit_product_page", id=request.form["id"]))
	
	p.name = name
	p.quantity = quantity
	p.min = min
	p.max = max
	
	db.session.add(p)
	db.session.commit()
	
	flash("Your changes have been saved.", "success")
	
	return redirect(url_for(".inventory"))
	
@main.route("/product/update-quantity", methods=["POST"])
@login_required
def update_product_quantity():
	p = Product.query.get(request.form["id"])
	if p is None:
		abort(404)
		
	try:
		new_quantity = int(request.form["new_quantity"])
	except ValueError:
		flash("Please enter a positive or negative numeric value.", "danger")
		return redirect(url_for(".inventory"))
	
	p.quantity = new_quantity
	db.session.add(p)
	db.session.commit()
	
	return redirect(url_for(".inventory"))
	
@main.route("/order/add", methods=["POST"])
@login_required
def new_order():
	options = json.loads(request.form["options"])
	o = Order(date=date_parse(options["date"]), owner=current_user)
	db.session.add(o)
	
	for product in options["products"]:
		po = ProductOrder(order=o, product=Product.query.get(product["product_id"]), quantity=int(product["quantity"]))
		product = Product.query.get(po.product.id)
		product.quantity += po.quantity
		db.session.add_all([po, product])
		
	db.session.commit()
	flash("Order successfully created!", "success")
	
	return redirect(url_for(".inventory"))
	
@main.route("/order/remove", methods=["POST"])
@login_required
def remove_order():
	id = json.loads(request.form["id"])
	o = Order.query.get_or_404(id)
	db.session.delete(o);
	db.session.commit()
	
	flash("Order successfully removed.", "success")
	return redirect(url_for(".inventory"))
	
@main.route("/order/edit")
@login_required
def edit_order_page():
	o = Order.query.get_or_404(request.args["id"])
	productorders = o.products
		
	return render_template("edit_order.html", order=o, productorders=productorders)
	
@main.route("/order/edit", methods=["POST"])
@login_required
def edit_order():
	o = Order.query.get_or_404(request.form["id"])

	try:
		date = request.form["date"]
		
		for key in request.form:
			if key.startswith("id") and key != "id":
				# print key, key[key.index('-')+1:]
				id = key[key.index('-')+1:]
				# print "id:", id, "\tquantity: ", "quantity-" + str(id)
				quantity = int(request.form["quantity-" + str(id)])
				
				po = ProductOrder().query.get_or_404((o.id, id))
				old_quantity = po.quantity
				po.quantity = quantity
				changed = quantity - old_quantity
				p = Product().query.get(po.product.id)
				p.quantity += changed
				db.session.add_all([po, p])
				
	except ValueError:
		flash("Could not read one or more submitted values. Make sure all input is correct and try again.", "danger")
		return redirect(url_for(".edit_order_page", id=o.id))

	db.session.commit()
	
	flash("Your changes have been saved.", "success")
	
	return redirect(url_for(".inventory"))
	
@main.route("/contact", methods=["GET", "POST"])
def contact():
	form = ContactForm()
	
	if form.validate_on_submit():
		name = form.name.data
		email = form.email.data
		subject = form.subject.data
		message = form.message.data
		
		# Do something here
		msg = Message("Inventory Manager - " + subject,
					  sender=email,
					  recipients=["mhumphree@gmail.com"])
		msg.body = "From: " + email + "\r\nName: " + name + "\r\n\r\n" + message
		mail.send(msg)
		
		flash("Your message has been saved. Thank you for your feedback!", "success")
		return redirect(url_for(".home"))
		
	else:
		for fieldName, errorMessages in form.errors.iteritems():
			for err in errorMessages:
				# do something with your errorMessages for fieldName
				flash("Error on field '{}': {}.".format(fieldName, err), "danger")
		
	return render_template("contact.html", form=form)
	