from flask.ext.wtf import Form
from wtforms import StringField, IntegerField, TextAreaField, SubmitField
from wtforms.validators import InputRequired, Optional, Length, NumberRange, Email
# from ..models import Product

class NewProductForm(Form):
	name	 = StringField("Name", validators=[InputRequired(), Length(1, 64)])
	quantity = IntegerField("Quantity", validators=[InputRequired(), NumberRange(min=0)])
	min		 = IntegerField("Minimum Quantity", validators=[Optional(), NumberRange(min=0)])
	max		 = IntegerField("Maximum Quantity", validators=[Optional(), NumberRange(min=1)])
	
	def validate_max(self, field):
		min = self.min.data
		max = self.max.data
		
		return min < max
		
class ContactForm(Form):
	name    = StringField("Your Name", validators=[InputRequired(), Length(1, 64)])
	email   = StringField("Email", validators=[Optional(), Length(1, 64), Email()])
	subject = StringField("Subject", validators=[Optional(), Length(1, 64)])
	message = TextAreaField("Message", validators=[InputRequired(), Length(1, 512)])
	submit  = SubmitField()