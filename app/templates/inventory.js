$SCRIPT_ROOT = {{ request.script_root|tojson|safe }};

var today = moment().format('YYYY-MM-DD');
$('#date').val(today);

$(function() {
	$(".list-group-item").click(function() {
		// Check to see if active
		var active = $(this).hasClass("active");
		
		// remove classes from all
		$(".list-group-item").removeClass("active");
		
		// add class to the one we clicked if was inactive
		if (!active) {
			$(this).addClass("active");
		}
   });
   
   // Bind modal submit button with form submit
	$("#submit-new-product").click(function(){
		$('#new-product-form').trigger('submit');
	});
	
	$("#new-product-form").validate({
		debug: false,
		
		rules: {
			name: {
				required: true,
				rangelength: [1, 64]
			},
			
			quantity: {
				required: true,
				integer: true,
				min: 0
			},
			
			min: {
				integer: true,
				min: 0
			},
			
			max: {
				integer: true,
				min: 1
			}
		}
	});
	
	// Remove product button
	$('#remove-product-button').click(function() {
		var product_id = null;
		
		$('#product-list .active').each(function(){
			product_id = $(this).data('id'); 
		});
		
		if (product_id != null) {
			post("{{ url_for('.remove_product') }}", params={id: product_id});
		}
	});
	
	// // Remove product button
	// $('#remove-product-button').click(function() {
		// var product_id = null;
		
		// $('#product-list .active').each(function(){
			// product_id = $(this).data('id'); 
		// });
		
		// if (product_id != null) {
			// post("{{ url_for('.remove_product') }}", params={id: product_id});
		// }
	// });
	
	$('#product-details-button').click(function (event) {
		var button = null;
		
		$("#product-list .active").each(function() {
			button = $(this);
		});
		
		if (button !== null) {
			// Extract info from data-* attributes
			var product = new Object();
			product.id = button.data('id');
			product.name = button.data("name");
			product.quantity = button.data("quantity");
			product.min = button.data("min");
			product.max = button.data("max");
			
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $("#product-details-modal");
			//modal.find('.modal-title').text('New message to ' + recipient)
			//modal.find('.modal-body').text()
			modal.find("#product-name").text(product.name);
			modal.find("#product-quantity").text(product.quantity);
			modal.find("#product-min").text(product.min);
			modal.find("#product-max").text(product.max);
			modal.modal("show");
		}
	});
	
	// Order details button
	$('#order-details-button').click(function (event) {
		var button = null;
		
		$("#order-list .active").each(function() {
			button = $(this);
		});
		
		if (button !== null) {
			// Extract info from data-* attributes
			var productorders = []
			var data = button.data("products");
			var lines = data.split("\n");
			
			for (var i=0; i<lines.length; i++) {
				var raw_product = lines[i].trim()
				if (raw_product === "")
					continue;
				
				var vals = raw_product.split(",");
				var productorder = new Object();
				productorder.id = vals[0];
				productorder.name = vals[1];
				productorder.quantity = vals[2];
				
				productorders.push(productorder);
			}
			
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $("#order-details-modal");
			modal.find("#order-date").text(button.data("date"));
			var tbody = modal.find("#productorder-table");
			tbody.empty();
			
			for (var i=0; i<productorders.length; i++) {
				var productorder = productorders[i];
				var td = $("<tr></tr>");
				td.append($("<td>" + productorder.name + "</td>"));
				td.append($("<td>" + productorder.quantity + "</td>"));
				tbody.append(td);
			}
			modal.modal("show");
		}
	});
	
	// Edit product button
	$("#edit-product-button").click(function() {
		var product_id = null;
		
		$("#product-list .active").each(function() {
			product_id = $(this).data("id");
		});
		
		if (product_id != null) {
			window.location = $SCRIPT_ROOT + "/edit?id=" + product_id;
		}
	});
	
	// Update quantity button
	$("#update-quantity-button").click(function() {
		var button = null;
		
		$("#product-list .active").each(function() {
			button = $(this);
		});
		
		if (button !== null) {
			// Extract info from data-* attributes
			var product = new Object();
			product.id = button.data('id');
			product.name = button.data("name");
			product.quantity = button.data("quantity");
			product.min = button.data("min");
			product.max = button.data("max");
			
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $("#update-quantity-modal");
			//modal.find('.modal-title').text('New message to ' + recipient)
			//modal.find('.modal-body').text()
			modal.find("#product-name").text(product.name);
			modal.find("#product-quantity").text(product.quantity);
			modal.find("#new-quantity").text(product.quantity);
			//modal.find("#new-quantity").oninput = function() {
			modal.find("input[name=quantity-change]").on("input", function() {
				modal.find("#new-quantity").text(parseInt(modal.find("#product-quantity").text(), 10) + parseInt(modal.find("input[name=quantity-change]").val(), 10))
			});;	
			modal.find("#product-min").text(product.min);
			modal.find("#product-max").text(product.max);			
			modal.modal("show");
		}
	});
	
	// Update quantity modal submit button
	$('#update-quantity-submit').click(function() {
		var product_id = null;
		
		$('#product-list .active').each(function(){
			product_id = $(this).data('id'); 
		});
		
		if (product_id != null) {
			post("{{ url_for('.update_product_quantity') }}", params={id: product_id, new_quantity: $("#update-quantity-modal").find("#new-quantity").text()});
		}
	});
	
	$("#add-product-order-button").click(function() {
		$("#new-product-order-list-container .form-inline").first().clone().appendTo("#new-product-order-list-container");
		$("#new-product-order-list-container .form-inline").last().find("input[type=number]").val(0);
	});
	
	$("#new-product-order-list-container").find("button").click(function() {
		$("#new-order-modal").modal("hide");
		$("#new-order-modal").on('hidden.bs.modal', function (e) {
			$("#new-product-modal").modal("show");
		});
	});
	
	// New Order Submit
	$("#new-order-submit").click(function() {
		var options = {};
		options.date = $("#date").val();
		options.products = [];
		
		$(".form-inline").each(function (index) {
			var product_order = {product_id: parseInt($(this).find("select").val()), quantity: $(this).find("input[type=number]").val()};
			
			if (!isNaN(product_order.product_id)) {
				options.products.push(product_order);
			}
		});

		post("{{ url_for('.new_order') }}", params={options: JSON.stringify(options)});
	});
	
	// Remove Order Button
	$("#remove-order-button").click(function() {
		var order_id = null;
		
		$('#order-list .active').each(function(){
			order_id = $(this).data('id'); 
		});
		
		if (order_id != null) {
			post("{{ url_for('.remove_order') }}", params={id: order_id});
		}
	});
	
	// Edit Order Button
	$("#edit-order-button").click(function() {
		var order_id = null;
		
		$("#order-list .active").each(function() {
			order_id = $(this).data("id");
		});
		
		if (order_id != null) {
			window.location = $SCRIPT_ROOT + "/order/edit?id=" + order_id;
		}
	});
});