from flask import Flask
import os

from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail
from flask_moment import Moment

basedir = os.path.abspath(os.path.dirname(__file__))

bootstrap = Bootstrap()
db = SQLAlchemy()
login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "auth.login"
moment = Moment()
mail = Mail()

def create_app():
	app = Flask(__name__)
	
	# SQLAlchemy
	app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(basedir, "data.sqlite")
	app.config["SQLALCHEMY_MIGRATE_REPO"] = os.path.join(basedir, 'db_repository')
	app.config["SQLALCHEMY_COMMIT_ON_TERARDOWN"] = True
	app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
	
	app.config["SECRET_KEY"] = "14E9DX9KazPyWf6k09l60F;18cFveu"
	
	app.config["MAIL_SERVER"] = "smtp.googlemail.com"
	app.config["MAIL_PORT"] = 587
	app.config["MAIL_USE_TLS"] = True
	app.config["MAIL_USERNAME"] = "mhumphree@gmail.com"
	app.config["MAIL_PASSWORD"] = "xpsktsfchuapgsfx"
	
	bootstrap.init_app(app)
	db.init_app(app)
	login_manager.init_app(app)
	moment.init_app(app)
	mail.init_app(app)
	
	from main import main as main_blueprint
	app.register_blueprint(main_blueprint)
	
	from .auth import auth as auth_blueprint
	app.register_blueprint(auth_blueprint)
	
	return app