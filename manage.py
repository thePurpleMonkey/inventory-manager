import os

from app import create_app, db
from app.models import Product, Order, ProductOrder, User
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager, Shell
from flask.ext.migrate import Migrate, MigrateCommand

basedir = os.path.abspath(os.path.dirname(__file__))

app = create_app()

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(basedir, "data.sqlite")
app.config["SQLALCHEMY_MIGRATE_REPO"] = os.path.join(basedir, 'db_repository')

db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)

def make_shell_context():
	return dict(app=app, db=db, User=User, Product=Product, Order=Order, ProductOrder=ProductOrder)
	
manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command("db", MigrateCommand)

if __name__ == "__main__":
	manager.run()